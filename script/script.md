## Opening

I am me and I am Jasmin/EC user

I got great help in preparing the talk from developers and they are in chat

Show what to expect slide.

Structure:

- Quick look at example
- A brief look at how Jasmin works
- Security proofs and correctness proofs in EasyCrypt

## Quick look at example

Show diagram

Show slide about AES NI  and AES in Jasmin

Show Jasmin code (both mem and reg)

Show how both can be called from C

Show safety check

Show constant-time, explain what it means and how it is checked

## Jasmin

Phylosophy:

- `quasm` inspired: more usable than assembly but still control (side effects explicit, flags)

- verification perspective: 

    - well defined semantics (two of them)
    - source level semantics (arrays as collections, loops, function calls)
    - verification close to structured languages such as C

- assembly in the head/control:

    - predict in your head what will come out
    - compiler is thin (can prove constant-time preservation)
    - a lot is left to the programmer (spilling, make sure register assignment exists)

Look at code AES

- explain exported functions vs inlineable functions
- high-level and low-level syntax (arrays, if, loops, functions vs registers, direct asm)
- show assembly and explain the context of *in-the-head*: unroll, inlined int
- explain stack, show stack code (explain that compiler takes control of stack)
- mention interpreter

## Proof

Show example slide, crypto spec => corresponding EC specification.

Show IND$-CPA, PRF and limitations => RFth, RPth, PRFth, NbEnc.

Show cheat sheet and explain that it shows main rules.

Show scheme, correctness and brush over proof.

## Implementation security

Show slides and discuss what it means.

Extraction, show results:

- Explain TCB (EC semantics for Jasmin match Coq, Coq semantics itself, plus extraction tool and EC itself)

- Explain AES_spec (operator) and what is proved about it

- Show how example uses AES and how one can recover the AES result as op

Proof of correctness:

- Highlight refined scheme uses AES op as f (assumption)

- Highlight refined scheme has same types, so we have a special case which is nice


