require import AllCore SmtMap List Distr DProd.
require (****) PRFth NbEnc NbPRFEnc.

(* Will work for arbitrary types *)

op q : int.

(* We assume a Scheme for a plaintext of a given size.
   We allow adversary to do 2*q queries to the encryption oracles *)
clone NbPRFEnc as NE1 with
  op q <- 2*q.

op flip0 : NE1.nonce -> NE1.nonce.
axiom flip0_neq n : flip0 n <> n.
axiom flip0_idem n : flip0 (flip0 n) = n.

op used_nonce n (nonces: NE1.nonce list) = 
  n \in nonces \/ flip0 n \in nonces.

type plaintext.

(* We assume that the type plaintext is in bijection with NE1.plaintext * NE1.plaintext *)   
op split_ : plaintext -> NE1.plaintext * NE1.plaintext.
op concat : NE1.plaintext * NE1.plaintext -> plaintext.

axiom split_K p1 p2 : split_ (concat(p1, p2)) = (p1, p2).
axiom concat_K p : concat (split_ p) = p.

(* Sample two NE1.plaintext and do the concatenation *)
op dciphertext = dmap (NE1.dciphertext `*` NE1.dciphertext) concat.

(* We build the NbEnc for a plaintext of 2 time the size of NE1.plaintext,
   We allow adversary to do q queries to the encryption oracles *)
clone include NbEnc with 
  type key        = NE1.key,
  type nonce      = NE1.nonce,
  type plaintext  <- plaintext,
  type ciphertext = plaintext,
  op dciphertext  <- dciphertext, 
  op used_nonce   <- used_nonce,
  op dflt         <- concat(NE1.dflt, NE1.dflt),
  op q <- q.

(* The concrete scheme simply apply the encryption scheme twise
   on sub component of the plaintext.
   Remark that we use a different nonce for the second encryption
   by flipping its lsb bit *)

module Scheme : Scheme_T = {

  proc kg = NE1.Scheme.kg

  proc enc(k:key, n:nonce, p:plaintext) : ciphertext = {
    var p1, p2, c1, c2;
    (p1,p2) <- split_ p;
    c1 <- NE1.Scheme.enc(k, n, p1); 
    c2 <- NE1.Scheme.enc(k, flip0 n, p2); 
    return concat(c1, c2);
  }

  proc dec(k:key, n:nonce, c:ciphertext) : plaintext = {
    var p;
    p <@ enc(k, n, c);
    return p;
  }
}.

module O2(O:NE1.OrclCPA) : OrclCPA = {
  proc enc(n:nonce, p:plaintext) : ciphertext = {
    var p1, p2, c1, c2;
    (p1, p2) <- split_ p;
    c1 <- O.enc(n, p1);
    c2 <- O.enc(flip0 n, p2);
    return concat (c1, c2);
  }
}.

module (A2(A:AdvCPA) : NE1.AdvCPA) (O:NE1.OrclCPA) = A(O2(O)).

module B2 (A:AdvCPA) = NE1.B(A2(A)).

op nonces2 (nonces:nonce list) = 
  flatten (map (fun n => [flip0 n; n]) nonces).

lemma size_nonces2 nonces: 
  size (nonces2 nonces) = 2 * size nonces.
proof.
  rewrite size_flatten -map_comp /(\o) /=.
  rewrite StdBigop.Bigint.sumzE StdBigop.Bigint.BIA.big_mapT /(\o) /=.
  by rewrite StdBigop.Bigint.big_constz count_predT.
qed.

lemma nonces2_in nonces n :
  n \in nonces2 nonces <=> used_nonce n nonces.
proof.
  rewrite -flatten_mapP; smt (flip0_idem).
qed.

section PROOF.

declare module A : AdvCPA {NE1.WO, WO, NE1.RealScheme, RealScheme, NE1.RF, NE1.RealPRF, NE1.Count}.

module IdealScheme2 = {
  proc init() : unit = {}
  
  proc enc(n : nonce, p : plaintext) : ciphertext = {
    var c1, c2;
    
    c1 <$ NE1.dciphertext;
    c2 <$ NE1.dciphertext;
    
    return concat(c1,c2);
  }
}.

equiv IdealE : IdealScheme.enc ~ IdealScheme2.enc : ={n,p} ==> ={res}.
proof.
  bypr (res{1}) (res{2}) => //.
  move=> &1 &2 a _.
  have -> : Pr[IdealScheme2.enc(n{2}, p{2}) @ &2 : res = a] = mu1 dciphertext a.
  + byphoare => //.
    proc.
    pose a1 := (split_ a).`1; pose a2 := (split_ a).`2.
    conseq (: c1 = a1 /\ c2 = a2); 1: smt (split_K concat_K).
    seq  1: (c1 = a1) (mu1 NE1.dciphertext a1) (mu1 NE1.dciphertext a2) _ 0%r true=> //=.
    + by rnd. + by rnd. + by hoare; auto=> /> ? ->.
    rewrite /dciphertext (dmap1E_can _ concat split_).
    + by apply concat_K.
    + by move=> [p1 p2] _; apply split_K.
    by rewrite -dprod1E /a1 /a2; case : (split_ a).
  by byphoare => //; proc; rnd; skip.
qed.

(* This is the proof that the cpa-advantage of A for Scheme (using q queries)
   is equal to the cpa-advantage of A2(A) for NE1.Scheme (using 2*q queries) *)
lemma cpa2_cpa &m :
`| Pr[CPA(A,RealScheme(Scheme)).main() @ &m : res] - 
       Pr[CPA(A,IdealScheme).main() @ &m : res]| =
`| Pr[NE1.CPA(A2(A),NE1.RealScheme(NE1.Scheme)).main() @ &m : res] - 
       Pr[NE1.CPA(A2(A),NE1.IdealScheme).main() @ &m : res]|.
proof.
have -> : Pr[CPA(A, RealScheme(Scheme)).main() @ &m : res] = 
          Pr[NE1.CPA(A2(A), NE1.RealScheme(NE1.Scheme)).main() @ &m : res].
+ byequiv => //.
  proc.
  call (: RealScheme.k{1} = NE1.RealScheme.k{2} /\ NE1.WO.nonces{2} = nonces2 WO.nonces{1}).
  + by proc; inline *; auto => />; smt(size_nonces2 nonces2_in flip0_neq flip0_idem).
  by inline *; auto.
have -> : Pr[CPA(A, IdealScheme).main() @ &m : res] = 
          Pr[CPA(A, IdealScheme2).main() @ &m : res].
+ byequiv => //.
  proc; sim (_ : true).
  by apply IdealE.
have -> // : Pr[CPA(A, IdealScheme2).main() @ &m : res] = 
             Pr[NE1.CPA(A2(A), NE1.IdealScheme).main() @ &m : res].
byequiv => //.
proc.
call (: NE1.WO.nonces{2} = nonces2 WO.nonces{1}).
+ proc.
  inline *.
  sp 1 0; if{1}.
  + rcondt{2} 5.   
    + by move=> &1; auto => />; smt(size_nonces2 nonces2_in).
    rcondt{2} ^if.
    + by move=> &1; auto => />; smt(size_nonces2 nonces2_in flip0_neq flip0_idem).
    by auto.
  rcondf{2} 5.   
  + by move=> &1; auto => />; smt(size_nonces2 nonces2_in).
  rcondf{2} ^if.
  + by move=> &1; auto => />; smt(size_nonces2 nonces2_in flip0_neq flip0_idem).
  by auto.
by inline *; auto.
qed.

lemma indcpa2_security &m : 
  `| Pr[CPA(A,RealScheme(Scheme)).main() @ &m : res] - 
       Pr[CPA(A,IdealScheme).main() @ &m : res]| =
  `|Pr[NE1.PRF(B2(A), NE1.RealPRF).main() @ &m : res] - Pr[NE1.PRF(B2(A), NE1.RF).main() @ &m : res]|.
proof.
  rewrite (cpa2_cpa &m).
  apply (NE1.indcpa_security (A2(A)) &m).
qed.

end section PROOF.




