require import AllCore Distr List.
from Jasmin require import JModel.

(* WARNING:
   If you have not do the exercise src/exercices/NbAESEnc2_ex_jazz 
   you should replace ALL OCCURENCES of 
       NbAESEnc2_ex_jazz
   by 
       NbAESEnc2_jazz

  The solution of this file can be found in 
     proof/example/NbAESEnc2_proof.ec.
*)
   
require import Array11 AES_jazz NbAESEnc2_ex_jazz AES_spec AES_proof.
require (*  *) NbPRFEnc2.

(* We refine our nonce-based encryption scheme by creating our own
   local copy of the theory and instantiating the PRF types and
   algorithm with those of an AES operator that defines the semantics
   of aes in Jasmin. 

   In general there could be an abstraction gap between the types used 
   by the refined scheme that we proved secure (e.g., ZModP) and the
   Jasmin representation of values in those types (e.g., W128.t). 
   In this case, preconditions/post-conditions assume/require
   that the inputs/outputs to/of the Jasmin implementation are 
   correct representations of the refined scheme inputs/outputs.
   Here representation is just equality. *)

op flip0 (n:W128.t) = n `^` mask1.
op split_ (n:W256.t) = (n \bits128 0, n \bits128 1).
op concat (p:W128.t*W128.t) = pack2 [p.`1; p.`2].

lemma flip0_0 n : (flip0 n).[0] = !n.[0]. 
proof. by rewrite /flip0 /= W128.of_intwE /= /int_bit /=. qed.

lemma flip0_i n i : 0 < i < 128 => (flip0 n).[i] = n.[i].
proof. 
  move => hi; rewrite /flip0 /= W128.of_intwE /int_bit /=.
  have /= /#: 2^1 <= 2^i.
  apply StdOrder.IntOrder.ler_weexpn2l => //= /#.
qed.

clone import NbPRFEnc2 as RefinedScheme with
  type NE1.key         = W128.t,
  type NE1.nonce       = W128.t,  
  type NE1.plaintext   <- W128.t,
  op   NE1.f           <- aes,
  op   NE1.dkey        <- W128.dword,
  op   NE1.dciphertext <- W128.dword,
  op   NE1.(^)         <- W128.(`^`),
  type plaintext       = W256.t,
  op   flip0           <- flip0,
  op   split_          <- split_,
  op   concat          <- concat
proof *.
realize NE1.dciphertext_ll by apply W128.dword_ll.
realize NE1.dciphertext_uni by apply W128.dword_uni.
realize NE1.dciphertext_full by apply W128.dword_fu.
realize NE1.xor_idempotent1 by smt(@W128).
realize NE1.xor_idempotent2 by smt(@W128).
realize flip0_neq by smt (flip0_0).
realize flip0_idem.
move=> n; apply W128.wordP; smt(flip0_0 flip0_i).
qed.
realize split_K by done.
realize concat_K.
by rewrite /concat /split_ /= => p; apply W2u128.allP.
qed.

(* Our library for AES includes a proof that extracted Jasmin code
   for the AES function (AES_jass.M, which calls the AES-NI instructions) is
   computing the aes operator that defines our reference AES 
   semantics. We first prove that our NbAESEncryption scheme is
   actually using the exact same Jasmin implementation, and
   derive as a corollary that M.aes computes the aes operator.  *)  

equiv keys_expand_correct_E : 
  NbAESEnc2_ex_jazz.M.keys_expand ~ AES_jazz.M.keys_expand :
    ={arg} ==> ={res}.
proof. by sim. qed.

phoare keys_expand_correct k : 
  [NbAESEnc2_ex_jazz.M.keys_expand : 
    arg = k ==> forall i, 0 <= i < 11 => res.[i] = key_i k i] = 1%r.
proof. 
  conseq keys_expand_correct_E (keys_expand_jazz_correct k) => /#. 
qed.

equiv aes_rounds_correct_E : 
  NbAESEnc2_ex_jazz.M.aes_rounds ~ AES_jazz.M.aes_rounds : ={arg} ==> ={res}.
proof. by sim. qed.

phoare rounds_correct k m : 
  [ NbAESEnc2_ex_jazz.M.aes_rounds : (forall i, 0 <= i < 11 => rkeys.[i] = key_i k i) /\ in_0 = m 
    ==>
    res = aes k m] = 1%r.
proof. by conseq aes_rounds_correct_E (aes_rounds_jazz_correct k m) => // /#. qed.

equiv aes_correct_E : 
  NbAESEnc2_ex_jazz.M.aes ~ AES_jazz.M.aes :
    ={arg} ==> ={res} by sim.

phoare aes_correct k n : [NbAESEnc2_ex_jazz.M.aes : arg = (k,n) ==> res  = aes k n] = 1%r.
proof. conseq aes_correct_E (aes_jazz_correct k n) => /#. qed.

equiv enc_correct_equiv : 
  NbAESEnc2_ex_jazz.M.enc_128 ~ NE1.Scheme.enc : ={arg} ==> ={res}.
proof.
proc; wp.
by ecall {1} (aes_correct k{1} n{1}).
qed.

(* With the above results as helpers, we can prove that our
   Jasmin code is correct with respect to the specification.
   We do this in two steps: first we prove equivalence to
   the scheme and then we derive correctness with respect
   to the encryption operator. *)

equiv enc_256_v1_correct_equiv : 
  NbAESEnc2_ex_jazz.M.enc_256_v1 ~ Scheme.enc :  ={arg} ==> ={res}.
proof.
(* FILL PROOF *)
admit.
qed.

module ConcreteScheme_v1 = {
  include Scheme [ kg ]
  proc enc(k:key, n:nonce, p:plaintext) = {
    var c;
    c <@ NbAESEnc2_ex_jazz.M.enc_256_v1(k, n, p);
    return c;
  }

  proc dec(k:key, n:nonce, c:ciphertext) = {
    var p;
    p <@ NbAESEnc2_ex_jazz.M.enc_256_v1(k, n, c);
    return p;
  }
}.

(* Now  we can use the correctness of the implementation
   propagate specification correctness to implementation
   correctness. *)

section.

declare module A : AdvCPA{NE1.RF, NE1.RealPRF, NE1.RealScheme, NE1.WO, NE1.Count, RealScheme, WO}.

lemma concrete_indcpa_security_v1 &m:
   (* Advantages match *)
  `| Pr[CPA(A,RealScheme(ConcreteScheme_v1)).main() @ &m : res] - 
       Pr[CPA(A,IdealScheme).main() @ &m : res]| =
  `| Pr[NE1.PRF(B2(A), NE1.RealPRF).main() @ &m : res] - 
       Pr[NE1.PRF(B2(A), NE1.RF).main() @ &m : res] |.
proof.
(* FILL PROOF *)
admit.
qed.

end section.

(* With the above results as helpers, we can prove that our
   Jasmin code (version 2) is correct with respect to the specification.
   We do this in two steps: first we prove equivalence to
   the scheme and then we derive correctness with respect
   to the encryption operator. *)

equiv enc_256_v2_correct_equiv : 
  NbAESEnc2_ex_jazz.M.enc_256_v2 ~ Scheme.enc :  ={arg} ==> ={res}.
proof.
(* FILL PROOF *)
admit.
qed.

module ConcreteScheme_v2 = {
  include Scheme [ kg ]
  proc enc(k:key, n:nonce, p:plaintext) = {
    var c;
    c <@ NbAESEnc2_ex_jazz.M.enc_256_v2(k, n, p);
    return c;
  }

  proc dec(k:key, n:nonce, c:ciphertext) = {
    var p;
    p <@ NbAESEnc2_ex_jazz.M.enc_256_v2(k, n, c);
    return p;
  }
}.

(* Now  we can use the correctness of the implementation
   propagate specification correctness to implementation
   correctness. *)

section.

declare module A : AdvCPA{NE1.RF, NE1.RealPRF, NE1.RealScheme, NE1.WO, NE1.Count, RealScheme, WO}.

lemma concrete_indcpa_security_v2 &m:
   (* Advantages match *)
  `| Pr[CPA(A,RealScheme(ConcreteScheme_v2)).main() @ &m : res] - 
       Pr[CPA(A,IdealScheme).main() @ &m : res]| =
  `| Pr[NE1.PRF(B2(A), NE1.RealPRF).main() @ &m : res] - 
       Pr[NE1.PRF(B2(A), NE1.RF).main() @ &m : res] |.
proof.
(* FILL PROOF *)
admit.
qed.



