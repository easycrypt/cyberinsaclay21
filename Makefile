JASMINC = jasminc # point to your jasminc
EASYCRYPT = easycrypt # point to your easycrypt

EXE= extraction/exercises
EE = extraction/example
AE = extraction/aeslib
EXB= build/exercises
EB = build/example
AB = build/aeslib
EXP= proof/exercises
EP = proof/example
AP = proof/aeslib
TT = test

OUT_DIR = build build/temp $(AB) $(EB) $(EXB)

.PHONY: dirs

all: dirs $(AB)/test_aes $(EB)/test_NbAESEnc $(EB)/test_NbAESEnc_mem \
	  $(EB)/NbAESEnc2.s $(EXB)/NbAESEnc2_ex.s \
	  $(AE)/AES_jazz.ec $(AE)/AES_jazz_ct.ec \
          $(EE)/NbAESEnc_jazz.ec $(EE)/NbAESEnc_jazz_ct.ec \
          $(EE)/NbAESEnc_mem_jazz.ec $(EE)/NbAESEnc_mem_jazz_ct.ec \
          $(EE)/NbAESEnc2_jazz.ec $(EE)/NbAESEnc2_jazz_ct.ec \
          $(EXE)/NbAESEnc2_ex_jazz.ec 

# BUILD AES LIB

$(AB)/aes.japp: src/aeslib/aes.jazz dirs
	cpp -nostdinc -DEXPORT_TEST src/aeslib/aes.jazz \
	    | grep -v "^#" > $(AB)/aes.japp

$(AB)/aes.s: $(AB)/aes.japp dirs
	$(JASMINC) $(AB)/aes.japp -o $(AB)/aes.s

# EXTRACT AES LIB FOR CORRECTNESS/SECURITY

$(AE)/AES_jazz.ec: $(AB)/aes.japp dirs
	cd build/temp && rm -f *.ec && \
    $(JASMINC) ../../$(AB)/aes.japp -ec aes -ec invaes -oec AES_jazz.ec
	mv build/temp/*.ec $(AE)

# EXTRACT AES LIB FOR CONSTANT-TIME

$(AE)/AES_jazz_ct.ec: $(AB)/aes.japp dirs
	cd build/temp && rm -f *.ec && \
	  $(JASMINC) ../../$(AB)/aes.japp -CT -ec aes -ec invaes -oec AES_jazz_ct.ec
	mv build/temp/*.ec $(AE)

# BUILD NbAESEnc WITH REGISTER CALLING CONVENTION

$(EB)/NbAESEnc.japp: src/example/NbAESEnc.jazz dirs
	cpp -nostdinc src/example/NbAESEnc.jazz  \
	    | grep -v "^#" > $(EB)/NbAESEnc.japp

$(EB)/NbAESEnc.s: $(EB)/NbAESEnc.japp dirs
	$(JASMINC) $(EB)/NbAESEnc.japp -o $(EB)/NbAESEnc.s

# EXTRACT NbAESEnc WITH REGISTER CALLING CONVENTION FOR CORRECTNESS/SECURITY

$(EE)/NbAESEnc_jazz.ec: $(EB)/NbAESEnc.japp dirs
	cd build/temp && rm -f *.ec && \
	  $(JASMINC) ../../$(EB)/NbAESEnc.japp -ec enc -ec dec -oec NbAESEnc_jazz.ec
	mv build/temp/*.ec $(EE)

# EXTRACT NbAESEnc WITH REGISTER CALLING CONVENTION FOR CONSTANT-TIME

$(EE)/NbAESEnc_jazz_ct.ec: $(EB)/NbAESEnc.japp dirs
	cd build/temp && rm -f *.ec && \
	  $(JASMINC) ../../$(EB)/NbAESEnc.japp -CT -ec enc -ec dec -oec NbAESEnc_jazz_ct.ec
	mv build/temp/*.ec $(EE)

# BUILD NbAESEnc WITH MEMORY CALLING CONVENTION

$(EB)/NbAESEnc_mem.japp: src/example/NbAESEnc_mem.jazz dirs
	cpp -nostdinc src/example/NbAESEnc_mem.jazz  \
	    | grep -v "^#" > $(EB)/NbAESEnc_mem.japp

$(EB)/NbAESEnc_mem.s: $(EB)/NbAESEnc_mem.japp dirs
	$(JASMINC) $(EB)/NbAESEnc_mem.japp -o $(EB)/NbAESEnc_mem.s

# EXTRACT NbAESEnc WITH MEMORY CALLING CONVENTION FOR CORRECTNESS/SECURITY

$(EE)/NbAESEnc_mem_jazz.ec: $(EB)/NbAESEnc_mem.japp dirs
	cd build/temp && rm -rf *.ec && \
	  $(JASMINC) ../../$(EB)/NbAESEnc_mem.japp -ec enc -ec dec -oec NbAESEnc_mem_jazz.ec
	mv build/temp/*.ec $(EE)

# EXTRACT NbAESEnc WITH MEMORY CALLING CONVENTION FOR CONSTANT-TIME

$(EE)/NbAESEnc_mem_jazz_ct.ec: $(EB)/NbAESEnc_mem.japp dirs
	cd build/temp && rm -rf *.ec && \
	  $(JASMINC) ../../$(EB)/NbAESEnc_mem.japp -CT -ec enc -ec dec -oec NbAESEnc_mem_jazz_ct.ec
	mv build/temp/*.ec $(EE)

# BUILD NbAESEnc2 WITH MEMORY CALLING CONVENTION

$(EB)/NbAESEnc2.japp: src/example/NbAESEnc2.jazz dirs
	cpp -nostdinc src/example/NbAESEnc2.jazz  \
	    | grep -v "^#" > $(EB)/NbAESEnc2.japp

$(EB)/NbAESEnc2.s: $(EB)/NbAESEnc2.japp dirs
	$(JASMINC) $(EB)/NbAESEnc2.japp -o $(EB)/NbAESEnc2.s

# EXTRACT NbAESEnc WITH MEMORY CALLING CONVENTION FOR CORRECTNESS/SECURITY

$(EE)/NbAESEnc2_jazz.ec: $(EB)/NbAESEnc2.japp dirs
	cd build/temp && rm -rf *.ec && \
	  $(JASMINC) ../../$(EB)/NbAESEnc2.japp -ec enc_128_ex -ec enc_256_v1 -ec enc_256_v2 -oec NbAESEnc2_jazz.ec
	mv build/temp/*.ec $(EE)

# EXTRACT NbAESEnc WITH MEMORY CALLING CONVENTION FOR CONSTANT-TIME

$(EE)/NbAESEnc2_jazz_ct.ec: $(EB)/NbAESEnc2.japp dirs
	cd build/temp && rm -rf *.ec && \
	  $(JASMINC) ../../$(EB)/NbAESEnc2.japp -CT -ec enc -ec dec -oec NbAESEnc2_jazz_ct.ec
	mv build/temp/*.ec $(EE)

# BUILD NbAESEnc2_ex WITH MEMORY CALLING CONVENTION

$(EXB)/NbAESEnc2_ex.japp: src/exercises/NbAESEnc2_ex.jazz dirs
	cpp -nostdinc src/exercises/NbAESEnc2_ex.jazz  \
	    | grep -v "^#" > $(EXB)/NbAESEnc2_ex.japp

$(EXB)/NbAESEnc2_ex.s: $(EXB)/NbAESEnc2_ex.japp dirs
	$(JASMINC) $(EXB)/NbAESEnc2_ex.japp -o $(EXB)/NbAESEnc2_ex.s

# EXTRACT NbAESEnc2_ex WITH MEMORY CALLING CONVENTION FOR CORRECTNESS/SECURITY

$(EXE)/NbAESEnc2_ex_jazz.ec: $(EXB)/NbAESEnc2_ex.japp dirs
	cd build/temp && rm -rf *.ec && \
	  $(JASMINC) ../../$(EXB)/NbAESEnc2_ex.japp -ec enc_128_ex -ec enc_256_v1 -ec enc_256_v2 -oec NbAESEnc2_ex_jazz.ec
	mv build/temp/*.ec $(EXE)

# BUILD TESTS

$(AB)/test_aes: $(AB)/aes.s $(TT)/test_aes.c dirs
	gcc -Wall $(AB)/aes.s $(TT)/test_aes.c \
	    -o $(AB)/test_aes

$(EB)/test_NbAESEnc: $(TT)/test_NbAESEnc.c $(EB)/NbAESEnc.s dirs
	gcc -msse4.1 -Wall $(EB)/NbAESEnc.s $(TT)/test_NbAESEnc.c \
	    -o $(EB)/test_NbAESEnc

$(EB)/test_NbAESEnc_mem: $(TT)/test_NbAESEnc_mem.c $(EB)/NbAESEnc_mem.s dirs
	gcc -Wall $(EB)/NbAESEnc_mem.s $(TT)/test_NbAESEnc_mem.c \
	    -o $(EB)/test_NbAESEnc_mem

test: all
	@echo
	@echo '************************************************'
	@echo '***        Testing AES implementation        ***'
	@echo '************************************************'
	$(AB)/test_aes
	@echo
	@echo '************************************************'
	@echo '*** Testing encryption scheme (reg cc)      *** '
	@echo '************************************************'
	$(EB)/test_NbAESEnc
	@echo
	@echo '************************************************'
	@echo '*** Testing encryption scheme (mem cc)      *** '
	@echo '************************************************'
	$(EB)/test_NbAESEnc_mem

safety: all
	@echo
	@echo '************************************************'
	@echo '***    Safety checking AES implementation    ***'
	@echo '************************************************'
	$(JASMINC) -checksafety $(AB)/aes.japp
	@echo
	@echo '************************************************'
	@echo '***Safety checking encryption scheme (reg cc)***'
	@echo '************************************************'
	$(JASMINC) -checksafety $(EB)/NbAESEnc.japp
	@echo
	@echo '************************************************'
	@echo '***Safety checking encryption scheme (mem cc)***'
	@echo '************************************************'
	$(JASMINC) -checksafety $(EB)/NbAESEnc_mem.japp

proofs: all
	@echo
	@echo '************************************************'
	@echo '***        Machine-checking AES proof        ***'
	@echo '************************************************'
	for f in $(AP)/*.ec; do echo $$f; \
	    $(EASYCRYPT) compile $$f -I $(AE); done
	@echo
	@echo '************************************************'
	@echo '*** Machine-checking encryption scheme proof ***'
	@echo '************************************************'
	for f in proof/example/*.eca; do echo $$f; \
	    $(EASYCRYPT) compile $$f -I $(AE) -I $(EE) -I $(AP); done
	for f in proof/example/*.ec; do echo $$f; \
	    $(EASYCRYPT) compile $$f -I $(AE) -I $(EE) -I $(AP); done

asmexo: $(EXB)/NbAESEnc2_ex.s

exercises: all
	@echo
	@echo '************************************************'
	@echo '*** Machine-checking exercises               ***'
	@echo '************************************************'
	for f in proof/exercises/*.eca; do echo $$f; \
	    $(EASYCRYPT) compile $$f -I $(AE) -I $(EE) -I $(AP) -I $(EP) -I $(EXE); done
	for f in proof/exercises/*.ec; do echo $$f; \
	    $(EASYCRYPT) compile $$f -I $(AE) -I $(EE) -I $(AP) -I $(EP) -I $(EXE); done

check: test safety proofs

clean: 
	rm -Rf $(AE)/* $(EE)/* $(EXE)/* build
	rm -f $(AP)/*.eco  
	rm -f $(EP)/*.eco
	rm -f $(EXP)/*.eco 

dirs: ${OUT_DIR}

${OUT_DIR}:
	mkdir -p ${OUT_DIR}

